---
layout: recipe
title:  "Cheesy Penne Casserole"
image: cheesy-penne-casserole.jpg
imagecredit:
date: 2023-01-03

authorName: Nadine Wilson
authorURL: nadines.recipes
sourceName: "Nadine's Recipes"
sourceURL: https://nadines.recipes/cheesy-penne-casserole/
category: dinner
cuisine: Italian
tags:
  - vegetarian
yield: 4
prepTime: 60
cookTime: 5

ingredients:
- 1 package penne
- 1/2 box spinach
- 1 jar Stefano's basil and tomato sauce (organic)
- 1 package Yves Italian tofu
- 1/2 yellow onion], chopped
- 1 clove garlic, slilced
- 2 Tbsp olive oil
- 1/2 lb cheddar
- 1/2 cup freshly ground parmesan

directions:
- Preheat oven to 500°F.
- In a sauce pan, add 1 Tbsp olive oil and garlic. Saute for 2 minutes.
- Add chopped onions. Saute for 2 minutes.
- Add tomato sauce. Cook over medium heat for 45 minutes.
- Cook penne to desired tenderness, then drain.
- In a medium frying pan, add 1 Tbsp olive oil and spinach. Saute for 5 minutes.
- Add tofu to sauce. Cook for 5 minutes.
- In a large casserole dish, mix penne, spinach and sauce.

---

Yummy!
