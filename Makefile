include .mk/GNUmakefile
hugo_RELEASE = 0.103.1

# See: https://gitlab.com/consensus.enterprises/drumkit/-/issues/109
ifeq ($(local_OS),Darwin)
    hugo_DOWNLOAD_URL = https://github.com/gohugoio/hugo/releases/download/v$(hugo_RELEASE)/hugo_extended_$(hugo_RELEASE)_darwin-universal.tar.gz
endif

default:
	hugo server

debug:
	hugo server -D

update-theme:
	git submodule update --remote --merge
